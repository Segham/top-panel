import { TopPanelBarRunesAllyState, TopPanelGraphicsState, TopPanelHealth, TopPanelItemsAllyState, TopPanelMana, TopPanelRoshanshowNextItems, TopPanelUltimateAllyState, TopPanelVisibilityDimHPMPState, TopPanelVisibilityFowTimeState } from "../menu";

TopPanelGraphicsState.OnValue(call => {
	if (call.selected_id !== 0)
		return
	TopPanelItemsAllyState.value = false
	TopPanelBarRunesAllyState.value = false
	TopPanelUltimateAllyState.value = false
	TopPanelRoshanshowNextItems.value = false
	TopPanelVisibilityFowTimeState.value = false
	TopPanelVisibilityDimHPMPState.value = false
	TopPanelMana.TopPanelBarAllyState.value = false
	TopPanelHealth.TopPanelBarAllyState.value = false
})
