import { EventsSDK } from "wrapper/Imports"
import { TOP_PANEL_DATA } from "../data"

EventsSDK.on("ParticleCreated", (id, path) => {

	if (path === "particles/items_fx/aegis_timer.vpcf" || path === "particles/items_fx/aegis_respawn_timer.vpcf")
		TOP_PANEL_DATA.AegisPickUpTime = 0

	if (path !== "particles/neutral_fx/roshan_spawn.vpcf")
		return

	TOP_PANEL_DATA.RoshanKillTime = 0
	TOP_PANEL_DATA.AegisPickUpTime = 0
})
