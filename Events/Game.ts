import { Entity, EntityManager, EventsSDK, GameState, Hero, Roshan } from "wrapper/Imports"
import { TOP_PANEL_DATA } from "../data"
import { TopPanelRoshanPingInteravl } from "../menu"

function RoshanHurt(name: string, obj: any) {
	if (name !== "entity_hurt" && name !== "entity_killed")
		return
	const ent1: Entity | number = EntityManager.EntityByIndex(obj.entindex_killed) || obj.entindex_killed,
		ent2: Entity | number = EntityManager.EntityByIndex(obj.entindex_attacker) || obj.entindex_attacker
	if (ent1 === undefined || ent2 === undefined
		|| (!(ent1 instanceof Hero && ent1.IsValid && !ent1.IsVisible) && !(ent2 instanceof Hero && ent2.IsValid && !ent2.IsVisible))
		|| (!(ent1 instanceof Roshan || ent1 === Roshan.Instance) && !(ent2 instanceof Roshan || ent2 === Roshan.Instance)))
		return
	const hero = ent1 instanceof Hero ? ent1 : ent2
	const ar = TOP_PANEL_DATA.RoshanAttacker.find(ar_ => ar_[0] === hero)
	if (ar === undefined) {
		TOP_PANEL_DATA.RoshanAttacker.push([ent1 instanceof Hero ? ent1 : ent2 as Entity, GameState.RawGameTime])
		TOP_PANEL_DATA.Sleeper.Sleep((TopPanelRoshanPingInteravl.value * 1000), "TOP_PANEL_ROSHAN")
	} else {
		ar[1] = GameState.RawGameTime
		TOP_PANEL_DATA.Sleeper.Sleep((TopPanelRoshanPingInteravl.value * 1000), "TOP_PANEL_ROSHAN")
		TOP_PANEL_DATA.RoshanCountAttack -= 1
	}
}

EventsSDK.on("GameEvent", (name, obj) =>
	RoshanHurt(name, obj))
