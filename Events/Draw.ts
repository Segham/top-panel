import { Ability, DOTAGameUIState_t, EventsSDK, GameState, Menu } from "wrapper/Imports"
import { TOP_PANEL_DATA } from "../data"
import { TopPanelBottom, TopPanelCenter, TopPanelCreateSettings, TopPanelDebug, TopPanelSide } from "../menu"
import { DrawNetWorthTeam, ModuleBuyBack, ModuleHPMPAllies, ModuleHPMPEnemies, ModuleItems, ModuleLastHiFowTimeEnemy, ModuleLastHitAlly, ModuleRoshan, ModuleRunes, ModuleScroll, ModuleUltimate } from "../Module/index"
import { TOP_PANEL_ERROR } from "../Service/ShowError"
import { TopPanel } from "../Service/TopPanel"
import { TOP_PANEL_UNIT } from "../Service/TopPanelUnit"
import { TOP_PANEL_VALIDATE } from "../Service/Validate"

EventsSDK.on("Draw", () => {

	TopPanelCreateSettings()

	if (!TOP_PANEL_VALIDATE.IsInGame || !TOP_PANEL_VALIDATE.IsValidSettings
		|| GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return

	TopPanel.CenterOnValueChange(TopPanelCenter!.value)
	TopPanel.SideOnValueChange(TopPanelSide!.value)
	TopPanel.BotOnValueChange(TopPanelBottom!.value)

	if (TopPanelDebug!.value)
		TopPanel.DebugDraw()

	if (!TOP_PANEL_DATA.AllowedMap.includes(GameState.MapName)) {
		TOP_PANEL_ERROR.TimeError(Menu.Localization.Localize("Top panel disabled. Map is not supported"), 15)
		return
	}

	if (TOP_PANEL_DATA.TCacheHero.size === 0 || TOP_PANEL_DATA.TCacheHero.size > 10) {
		TOP_PANEL_ERROR.TimeError(Menu.Localization.Localize("Top panel disabled. Needed 10 players"), 10)
		return
	}

	TOP_PANEL_DATA.TCacheHero.forEach(data => RenderPanel(data[0], data[1]))

	ModuleRoshan()
	DrawNetWorthTeam()
})

const RenderPanel = (data: TOP_PANEL_UNIT, ability: Ability) => {
	if (data === undefined || !data.hero.IsValid || ability === undefined || !ability.IsValid)
		return

	let index = 0
	let index2 = 0

	const Items = data.hero.Items

	if (!data.hero.IsEnemy()) {

		ModuleLastHitAlly(data)
		index = ModuleHPMPAllies(data, index)
		index = ModuleBuyBack(data, index)
		index = ModuleUltimate(data, index, ability)
		index = ModuleScroll(data, index)
		index = ModuleItems(data, Items, index)
		ModuleRunes(data, index)
		return
	}

	ModuleLastHiFowTimeEnemy(data)
	index2 = ModuleHPMPEnemies(data, index2)
	index2 = ModuleBuyBack(data, index2)
	index2 = ModuleUltimate(data, index2, ability)
	index2 = ModuleScroll(data, index2)
	index2 = ModuleItems(data, Items, index2)
	ModuleRunes(data, index2)
}
