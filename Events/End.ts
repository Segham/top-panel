import { EventsX } from "X-Core/Imports"
import { TOP_PANEL_DATA } from "../data"
import { TOP_PANEL_ERROR } from "../Service/ShowError"

EventsX.on("GameEnded", () => {
	TOP_PANEL_DATA.Dispose()
	TOP_PANEL_ERROR.Sleeper.FullReset()
})
