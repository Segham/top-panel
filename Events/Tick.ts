import { Ability, ABILITY_TYPES, dark_willow_bedlam, EventsSDK, GameState } from "wrapper/Imports"
import { TOP_PANEL_DATA } from "../data"
import { TOP_PANEL_UNIT } from "../Service/TopPanelUnit"
import { TOP_PANEL_VALIDATE } from "../Service/Validate"

EventsSDK.on("PostDataUpdate", () => {
	if (!TOP_PANEL_VALIDATE.IsInGame || TOP_PANEL_DATA.AllowedMap.indexOf(GameState.MapName) === -1)
		return
	TOP_PANEL_DATA.Heroes = TOP_PANEL_DATA.CHeroes
		.filter(hero => TOP_PANEL_VALIDATE.IsRealHero(hero))
	TOP_PANEL_DATA.Heroes.forEach(hero => {
		if (TOP_PANEL_DATA.Heroes.length === 0 || TOP_PANEL_DATA.Heroes.length > 10) {
			if (TOP_PANEL_DATA.TCacheHero.size !== 0)
				TOP_PANEL_DATA.TCacheHero.clear()
			return
		}
		const cache = TOP_PANEL_DATA.TCacheHero.get(hero)
		if (cache === undefined) {
			const abililty = hero.Spells.find(abil => abil !== undefined
				&& (abil.AbilityType === ABILITY_TYPES.ABILITY_TYPE_ULTIMATE)
				&& !(abil instanceof dark_willow_bedlam)) as Ability
			TOP_PANEL_DATA.TCacheHero.set(hero, [new TOP_PANEL_UNIT(hero), abililty])
		}
	})
})
