import { DOTA_CHAT_MESSAGE, EventsSDK, GameState } from "wrapper/Imports"
import { TOP_PANEL_DATA } from "../data"

EventsSDK.on("ChatEvent", (type: DOTA_CHAT_MESSAGE) => {
	if (type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_ROSHAN_KILL && type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_AEGIS)
		return

	TOP_PANEL_DATA.Sleeper.ResetKey("TOP_PANEL_ROSHAN")
	TOP_PANEL_DATA.Sleeper.ResetKey("TOP_PANEL_ROSHAN_ATTACK")

	if (type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_ROSHAN_KILL) {
		TOP_PANEL_DATA.RoshanKillTime = GameState.RawGameTime
		TOP_PANEL_DATA.Roshan = undefined
		TOP_PANEL_DATA.RoshanCountAttack = 7
		TOP_PANEL_DATA.RoshansKilledCount += 1
		return
	}

	if (type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_AEGIS)
		return

	TOP_PANEL_DATA.AegisPickUpTime = GameState.RawGameTime
})
