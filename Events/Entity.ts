import { ArrayExtensions, EventsSDK, Hero, Roshan, RoshanSpawner } from "wrapper/Imports"
import { TOP_PANEL_DATA } from "../data"

EventsSDK.on("EntityCreated", ent => {

	if (ent instanceof RoshanSpawner)
		TOP_PANEL_DATA.RoshanSpawners = ent

	if (ent instanceof Roshan)
		TOP_PANEL_DATA.Roshan = ent

	if (ent instanceof Hero)
		TOP_PANEL_DATA.CHeroes.push(ent)
})

EventsSDK.on("EntityDestroyed", ent => {

	if (ent instanceof Roshan)
		TOP_PANEL_DATA.Roshan = ent

	if (ent instanceof RoshanSpawner)
		TOP_PANEL_DATA.RoshanSpawners = undefined

	if (ent instanceof Hero)
		ArrayExtensions.arrayRemove(TOP_PANEL_DATA.CHeroes, ent)
})
