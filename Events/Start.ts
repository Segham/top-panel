import { EventsSDK } from "wrapper/Imports"
import { TOP_PANEL_ERROR } from "../Service/ShowError"

EventsSDK.on("GameStarted", () => {
	TOP_PANEL_ERROR.ShowError = false
})
