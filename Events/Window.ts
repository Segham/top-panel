import { Vector2 } from "wrapper/Imports"
import { EventsX } from "X-Core/Imports"
import { TopPanelBottom, TopPanelCenter, TopPanelSide } from "../menu"
import { TOP_PANEL_VALIDATE } from "../Service/Validate"

EventsX.on("WindowChange", state => {
	if (!TOP_PANEL_VALIDATE.IsValidSettings)
		return

	const num = Math.floor(state.x * 0.0535)
	const size2F = new Vector2(Math.floor(state.x * 0.1605), Math.floor(state.y * 0.0375))

	TopPanelCenter!.value = num
	TopPanelSide!.value = Math.round(size2F.x)
	TopPanelBottom!.value = Math.round(size2F.y)
})
