import { EventsSDK, GameState } from "wrapper/Imports"
import { TOP_PANEL_DATA } from "../data"

EventsSDK.on("ModifierCreated", modifire => {
	if (!TOP_PANEL_DATA.ModifreRunes.includes(modifire.Name))
		return
	TOP_PANEL_DATA.ModifiersRuneMap.set(
		modifire,
		GameState.RawGameTime + modifire.RemainingTime,
	)
})

EventsSDK.on("ModifierRemoved", modifire => {
	if (!TOP_PANEL_DATA.ModifreRunes.includes(modifire.Name))
		return
	TOP_PANEL_DATA.ModifiersRuneMap.delete(modifire)
})
