import { GameRules, GameState, Input, InputEventSDK, Menu, VMouseKeys } from "wrapper/Imports"
import { TOP_PANEL_DATA } from "../data"
import { TopPanelRoshanaltKey, TopPanelRoshanRemaining, TopPanelRoshanState, TopPanelRoshanUseChat } from "../menu"
import { TOP_PANEL_UTIL } from "../Service/Util"
import { TOP_PANEL_VALIDATE } from "../Service/Validate"

InputEventSDK.on("MouseKeyDown", key => {
	if (!TOP_PANEL_VALIDATE.IsInGame
		|| !TopPanelRoshanUseChat.value
		|| !TopPanelRoshanState.value
		|| key !== VMouseKeys.MK_LBUTTON)
		return true

	const rec = TOP_PANEL_DATA.RoshanRectangleX
	const cursorPosition = Input.CursorOnScreen
	const cursorInRosh = cursorPosition.IsUnderRectangle(rec.X, rec.Y, rec.Width, rec.Height)

	const gameTime = GameRules!.GameTime
	const killTime = GameState.RawGameTime - TOP_PANEL_DATA.RoshanKillTime
	const timeToEndPickUp = GameState.RawGameTime - TOP_PANEL_DATA.AegisPickUpTime

	let flag = TopPanelRoshanRemaining.value

	if (TopPanelRoshanaltKey.is_pressed)
		flag = !flag

	let TextChat = ""

	if (killTime > 480) {

		let killTimePredict = 660 - killTime

		if (!flag)
			killTimePredict += gameTime

		TextChat = `${TOP_PANEL_UTIL.sToMin(killTimePredict, true)}*`

	} else if (TOP_PANEL_DATA.AegisPickUpTime !== 0 && timeToEndPickUp <= 300) {
		let num4 = 300 - timeToEndPickUp

		if (!flag)
			num4 += gameTime

		TextChat = `${Menu.Localization.Localize("aegis")} ${TOP_PANEL_UTIL.sToMin(num4, true)}`

	} else {
		let num6 = 660 - killTime

		if (!flag)
			num6 += gameTime

		TextChat = `${Menu.Localization.Localize("rosh")} ${TOP_PANEL_UTIL.sToMin((num6 - 180), true)} ${TOP_PANEL_UTIL.sToMin(num6, true)}`
	}

	if (!cursorInRosh || TOP_PANEL_DATA.RoshanKillTime === 0)
		return true

	GameState.ExecuteCommand(`say_team ${TextChat}`)
	return false
})
