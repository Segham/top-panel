import { Ability, Color, Entity, GameSleeper, Hero, Modifier, Roshan, RoshanSpawner } from "wrapper/Imports"
import { RectangleX } from "../X-Core/Imports"
import { TOP_PANEL_UNIT } from "./Service/TopPanelUnit"

export enum TOP_PANEL_GRAPHICS {
	MINIMAL,
	AVERAGE,
	MAXIMUM,
}

export class TOP_PANEL_DATA {

	public static Heroes: Hero[] = []
	public static CHeroes: Hero[] = []

	public static RoshanKillTime = 0
	public static AegisPickUpTime = 0

	public static RoshanCountAttack = 14
	public static RoshansKilledCount = 1

	public static Roshan: Nullable<Roshan>
	public static RoshanDrop: string[] = ["item_aegis"]

	public static Sleeper = new GameSleeper()
	public static RoshanSpawners: Nullable<RoshanSpawner>

	public static RoshanAttacker: [Entity, number][] = []
	public static RoshanRectangleX = new RectangleX()
	public static TCacheHero = new Map<Hero, [TOP_PANEL_UNIT, Ability]>()
	public static AllowedMap: string[] = ["start", "dota", "hero_demo_main"]
	public static ModifiersRuneMap: Map<Modifier, number> = new Map()

	public static ColorCorrection = {
		topbar: Color.Black.SetA(210),
		colorBlind: new Color(102, 237, 237),
	}

	public static ModifreRunes: string[] = [
		"modifier_rune_arcane",
		"modifier_rune_doubledamage",
		"modifier_rune_haste",
		"modifier_rune_invis",
		"modifier_rune_regen",
	]

	public static ExcludeUnitNameLH: string[] = [
		"npc_dota_gyrocopter_homing_missile",
		"npc_dota_techies_remote_mine",
		"npc_dota_techies_land_mine",
		"npc_dota_techies_stasis_trap",
		"npc_dota_weaver_swarm",
	]

	public static Dispose() {
		this.Heroes = []
		this.CHeroes = []
		this.TCacheHero.clear()
		this.Sleeper.FullReset()
		this.Roshan = undefined
		this.RoshanKillTime = 0
		this.AegisPickUpTime = 0
		this.RoshansKilledCount = 1
		this.RoshanAttacker = []
		this.RoshanCountAttack = 14
		this.ModifiersRuneMap.clear()
		this.RoshanSpawners = undefined
		this.RoshanDrop = ["item_aegis"]
	}
}
