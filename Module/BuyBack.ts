import { TopPanelBarBuyBackCooldown, TopPanelBarBuyBackState, TopPanelBarEnemySize } from "../menu"
import { TopPanel } from "../Service/TopPanel"
import { TOP_PANEL_UNIT } from "../Service/TopPanelUnit"
import { TOP_PANEL_VALIDATE } from "../Service/Validate"
import { Input, LocalPlayer, PlayerResource, VKeys } from "../wrapper/Imports"

export const ModuleBuyBack = (Data: TOP_PANEL_UNIT, index: number) => {
	if (!TopPanelBarBuyBackState.value
		|| PlayerResource === undefined
		|| (!Data.hero.IsEnemy() && TOP_PANEL_VALIDATE.IsWaitStartGame))
		return index

	const Ratio = TopPanel.ScreenRatio
	const playerID = Data.PlayerID
	const SizeScaleBars = TopPanelBarEnemySize.value

	const Position = TopPanel.GPBPosition(playerID, Math.floor(15 + (index / 2) * Ratio))
	const Position2 = TopPanel.GPBPosition(playerID, Math.floor(15 + (index / 2) * Ratio), 2)

	Data.ForceDrawBuyback(Position, Position2)
	index += 5

	if (Data.ForceDrawBuybackCooldown(Position2, (!Data.hero.IsAlive ? 22 : index), TopPanelBarBuyBackCooldown))
		index += 15 + (SizeScaleBars * Ratio)

	if (Data.PlayerID === LocalPlayer!.PlayerID && PlayerResource.PlayerData[Data.PlayerID].IsPlusSubscriber && Input.IsKeyDown(VKeys.MENU))
		index += 15 * Ratio

	return index
}
