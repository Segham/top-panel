import { TopPanelBarRunesAllyState, TopPanelBarRunesEnemyState } from "../menu"
import { TopPanel } from "../Service/TopPanel"
import { TOP_PANEL_UNIT } from "../Service/TopPanelUnit"

export const ModuleRunes = (Data: TOP_PANEL_UNIT, index: number) => {
	if ((!TopPanelBarRunesAllyState.value && !Data.hero.IsEnemy())
		|| (!TopPanelBarRunesEnemyState.value && !Data.hero.IsEnemy()))
		return index

	const Ratio = TopPanel.ScreenRatio
	const playerID = Data.PlayerID
	const position = TopPanel.GPBPosition(playerID, Math.floor(15 * Ratio), index + Math.floor(5 * Ratio))
	Data.DrawRunes(position)
	return index
}
