import { TopPanelBlindState, TopPanelNetWorthColor, TopPanelNetWorthState } from "../menu";
import { TopPanel } from "../Service/TopPanel";
import { Color, LocalPlayer, RendererSDK, Team, Vector2 } from "../wrapper/Imports";
import { PathX, PlayerX, RectangleDraw } from "../X-Core/Imports";

export let DrawNetWorthTeam = () => {
	if (!TopPanelNetWorthState.value)
		return

	let direNetwoth = 0
	let radiantNetworh = 0
	const Ratio = TopPanel.ScreenRatio
	PlayerX.DataX.forEach((playerX, unit) => unit.Team === Team.Radiant ? radiantNetworh += playerX.NetWorth : direNetwoth += playerX.NetWorth)

	let team: Team = Team.None
	let text: Nullable<string>
	const num3 = radiantNetworh - direNetwoth

	if (num3 > 0) {
		team = Team.Radiant
		text = `≈ ${Math.round(num3 / 1000)} k`
	} else {
		team = Team.Dire
		text = `≈ ${Math.round(num3 / -1000)} k`
	}

	const position = TopPanel.GetScorePosition(team)
	position.Y += (position.Height)
	position.Height = Math.floor(22 * Ratio)

	const fontSize = Math.floor(15 * Ratio)
	const num2Size = ((position.Width - (RendererSDK.GetTextSize(text, "Calibri", fontSize).x + 24 * Ratio)) / 2)

	RectangleDraw.Image(PathX.Images.chat_preview_opacity_mask, position, TopPanelNetWorthColor.selected_color)

	const imageTeam = LocalPlayer!.Team === team
		? (!TopPanelBlindState.value ? PathX.Images.arrow_gold_dif : PathX.Images.arrow_gold_dif_blind)
		: PathX.Images.arrow_plus_stats_red

	RendererSDK.Image(imageTeam,
		new Vector2(position.X + num2Size, position.Y + Math.floor(4 * Ratio)),
		-1,
		new Vector2(12 * Ratio, 12 * Ratio),
	)

	RendererSDK.Text(text,
		new Vector2(
			(position.X + num2Size + Math.floor(15 * Ratio)),
			(position.Y + 1.5 + Ratio),
		),
		Color.White,
		"Calibri",
		fontSize,
	)
}
