import { Input, VKeys } from "wrapper/Imports"
import { TopPanelStatusAlliesLHState, TopPanelStatusEnemyLHState, TopPanelVisibilityFowTimeState } from "../menu"
import { TopPanel } from "../Service/TopPanel"
import { TOP_PANEL_UNIT } from "../Service/TopPanelUnit"

export const ModuleLastHiFowTimeEnemy = (Data: TOP_PANEL_UNIT) => {
	const Ratio = TopPanel.ScreenRatio
	const playerID = Data.PlayerID

	if (TopPanelVisibilityFowTimeState.value) {
		if (!Input.IsKeyDown(VKeys.MENU) && !Data.hero.IsVisible) {
			Data.DrawFowTime(TopPanel.GPPosition(playerID), Ratio)
		} else {
			TopPanelStatusEnemyLHState.value
				? Data.LastHitDeny(TopPanel.GPPosition(playerID), Ratio)
				: Data.DrawFowTime(TopPanel.GPPosition(playerID), Ratio)
		}
	} else {
		if (TopPanelStatusEnemyLHState.value)
			Data.LastHitDeny(TopPanel.GPPosition(playerID), Ratio)
	}
}
export const ModuleLastHitAlly = (Data: TOP_PANEL_UNIT) => {
	if (!TopPanelStatusAlliesLHState.value)
		return
	const Ratio = TopPanel.ScreenRatio
	const playerID = Data.PlayerID
	Data.LastHitDeny(TopPanel.GPPosition(playerID), Ratio)
}
