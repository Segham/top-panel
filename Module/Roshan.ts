import { Color, GameRules, GameState, Input, LocalPlayer, Menu, Player, RendererSDK, Roshan, SoundSDK, Team, Vector2, VKeys } from "wrapper/Imports"
import { GameX, PathX, RectangleDraw, RectangleX } from "X-Core/Imports"
import { TOP_PANEL_DATA } from "../data"
import { TopPanelBlindState, TopPanelRoshanaltKey, TopPanelRoshanPing, TopPanelRoshanPingInteravl, TopPanelRoshanPingSoundVolume, TopPanelRoshanRemaining, TopPanelRoshanshowMinTime, TopPanelRoshanshowNextItems, TopPanelRoshanState, TopPanelRoshanUseScan } from "../menu"
import { TopPanel } from "../Service/TopPanel"
import { TOP_PANEL_UTIL } from "../Service/Util"

export const ModuleRoshan = () => {
	if (!TopPanelRoshanState.value)
		return

	const rec = TopPanel.GetTimePosition
	const rawGameTime = GameState.RawGameTime
	const IsAltAlive = !Input.IsKeyDown(VKeys.MENU) && TOP_PANEL_DATA.RoshanKillTime === 0 && !TOP_PANEL_DATA.Sleeper.Sleeping("TOP_PANEL_ROSHAN")

	PingNotify(rawGameTime, rec)

	rec.Y += (100 * TopPanel.ScreenRatio)
	rec.MultiplySizeF(new Vector2(0.45, Input.IsKeyDown(VKeys.MENU) ? 0.95 : 0))

	TOP_PANEL_DATA.RoshanRectangleX = rec

	if (IsAltAlive)
		return

	const vector = rec.Center.Add(new Vector2(0, rec.Height * 0.65))

	TimeRespawn(vector.Clone())

	RoshanAttackIcon(vector.Clone(), rawGameTime)

	if (!TopPanelRoshanshowNextItems.value)
		return

	ShowNextItems(vector.Clone())
}

function TimeRespawn(vector: Vector2) {
	if (TOP_PANEL_DATA.RoshanKillTime === 0)
		return

	const gameTime = GameRules!.GameTime
	const rawGameTime = GameState.RawGameTime
	const killTime = rawGameTime - TOP_PANEL_DATA.RoshanKillTime
	const timeToEndPickUp = rawGameTime - TOP_PANEL_DATA.AegisPickUpTime

	let flag = TopPanelRoshanRemaining.value

	if (TopPanelRoshanaltKey.is_pressed)
		flag = !flag

	let Text = ""

	if (killTime > 480) {

		let killTimePredict = 660 - killTime

		if (!flag)
			killTimePredict += gameTime

		Text = `${TOP_PANEL_UTIL.sToMin(killTimePredict)}*`

	} else if (TOP_PANEL_DATA.AegisPickUpTime !== 0 && timeToEndPickUp <= 300) {
		let num4 = 300 - timeToEndPickUp

		if (!flag)
			num4 += gameTime

		Text = `${Menu.Localization.Localize("Aegis")}: ${TOP_PANEL_UTIL.sToMin(num4)}`

	} else {
		let num6 = 660 - killTime

		if (!flag)
			num6 += gameTime

		Text = TOP_PANEL_UTIL.sToMin(num6)

		if (TopPanelRoshanshowMinTime.value)
			Text = `${TOP_PANEL_UTIL.sToMin((num6 - 180))} - ${Text}`
	}

	const Size = RendererSDK.GetTextSize(Text, "Calibri", 18, 600).DivideScalar(2)
	const vec = vector.SubtractForThis(Vector2.FromVector3(Size.SubtractScalarX(5)))
		.AddScalarY(TopPanelRoshanshowNextItems.value ? 30 : 10)

	RendererSDK.Text(Text, vec, Color.White, "Calibri", 18, 600)
}

function ShowNextItems(vector: Vector2) {
	const roshan = TOP_PANEL_DATA.Roshan

	if (roshan === undefined || roshan.Items.length <= 0) {

		if (TOP_PANEL_DATA.RoshansKilledCount === 2 && !TOP_PANEL_DATA.RoshanDrop.includes("item_cheese"))
			TOP_PANEL_DATA.RoshanDrop.push("item_cheese")

		if (TOP_PANEL_DATA.RoshansKilledCount === 3 && !TOP_PANEL_DATA.RoshanDrop.includes("item_ultimate_scepter"))
			TOP_PANEL_DATA.RoshanDrop.push("item_ultimate_scepter")

		if (TOP_PANEL_DATA.RoshansKilledCount > 3 && !TOP_PANEL_DATA.RoshanDrop.includes("item_refresher_shard"))
			TOP_PANEL_DATA.RoshanDrop.push("item_refresher_shard")

	}

	const size = (20 * TopPanel.ScreenRatio)
	const size2 = (4 * TopPanel.ScreenRatio)
	const left = new Vector2(vector.x - (size / 2) * TOP_PANEL_DATA.RoshanDrop.length - (size2 / 2) * (TOP_PANEL_DATA.RoshanDrop.length - 1), vector.y)

	for (let index = 0; index < TOP_PANEL_DATA.RoshanDrop.length; index++) {
		const abil = TOP_PANEL_DATA.RoshanDrop[index]
		RectangleDraw.Image(
			PathX.DOTAItems(abil),
			new RectangleX(left.Add(new Vector2(index * size + index * size2, 0)), new Vector2(size, size)),
			Color.White, 0,
		)

		RectangleDraw.Image(PathX.Images.buff_outline,
			new RectangleX(left.Add(new Vector2(index * size + index * size2, 0)),
				new Vector2(size, size),
			),
		)
	}
}

function PingNotify(rawGameTime: number, rec: RectangleX) {

	TOP_PANEL_DATA.RoshanAttacker = TOP_PANEL_DATA.RoshanAttacker.filter(ar => rawGameTime - ar[1] < 2)

	if (TOP_PANEL_DATA.RoshanAttacker.length === 0 || Player === undefined)
		return

	const roshanPos = TOP_PANEL_DATA.RoshanSpawners
	const vector = rec.Center.Add(new Vector2(0, rec.Height * 3.2))

	const roshHP = Roshan.HP !== 0 ? Menu.Localization.Localize("HP: ")
		+ `${Math.floor(Roshan.HP)}/${Math.floor(Roshan.MaxHP)} | ${Math.floor(Math.floor(Roshan.HP) / Math.floor(Roshan.MaxHP) * 100)}%` : ""

	const RoshText = Menu.Localization.Localize("Roshan is under attack ") + roshHP
	const textSize = Vector2.FromVector3(RendererSDK.GetTextSize(RoshText, "Calibri", 18, 600))
	const VectorPosition = vector.SubtractForThis(textSize.DivideScalar(2)).AddScalarY((Input.IsKeyDown(VKeys.MENU) ? 28 : 5))

	RendererSDK.Text(
		RoshText,
		VectorPosition,
		Color.White,
		"Calibri",
		18,
		600,
	)

	if (roshanPos === undefined)
		return

	if (TOP_PANEL_DATA.Sleeper.Sleeping("TOP_PANEL_ROSHAN_ATTACK"))
		return

	if (TopPanelRoshanUseScan.value && TOP_PANEL_DATA.RoshanCountAttack <= 0) {

		const alliesCountAlive = TOP_PANEL_DATA.CHeroes.filter(x => !x.IsEnemy() && x.IsAlive)

		const scanCooldown = LocalPlayer!.Team === Team.Radiant
			? GameRules!.ScanCooldownRadiant
			: GameRules!.ScanCooldownDire

		if (alliesCountAlive.length >= 3 && scanCooldown === 0 && !TOP_PANEL_DATA.Sleeper.Sleeping("TOP_PANEL_ROSHAN_ATTACK")) {
			LocalPlayer!.Scan(TOP_PANEL_UTIL.RandomVector3(roshanPos.Position.Clone(), 50, 100))
			TOP_PANEL_DATA.Sleeper.Sleep((GameX.Ping / 1000) + 500, "TOP_PANEL_ROSHAN_SCAN")
		}
	}

	if (TopPanelRoshanPingSoundVolume.value !== 0) {
		SoundSDK.PlaySound("sounds/ui/ping_attack.vsnd_c", (TopPanelRoshanPingSoundVolume.value / 100))
		TOP_PANEL_DATA.Sleeper.Sleep((TopPanelRoshanPingInteravl.value * 1000), "TOP_PANEL_ROSHAN_ATTACK")
	}

	if (!TopPanelRoshanPing.value)
		return

	RendererSDK.DrawMiniMapPing(TOP_PANEL_UTIL.RandomVector3(roshanPos.Position.Clone(), 50, 100), Color.White, (rawGameTime + 10), roshanPos.Position.Length)
}

function RoshanAttackIcon(vector: Vector2, rawGameTime: number) {

	const size = (20 * TopPanel.ScreenRatio)
	const size2 = (4 * TopPanel.ScreenRatio)
	const timeToEnd = rawGameTime - TOP_PANEL_DATA.RoshanKillTime
	const center = new Vector2(vector.x - (size / 2) - size2, vector.y)
	const position = new RectangleX(center.Subtract(new Vector2(3, size + 20)), new Vector2(size * 1.7, size * 1.7))

	if (timeToEnd > 660 || TOP_PANEL_DATA.RoshanKillTime === 0) {
		RectangleDraw.Image(PathX.Images.buff_outline,
			position,
			TOP_PANEL_DATA.Sleeper.Sleeping("TOP_PANEL_ROSHAN_ATTACK")
				? Color.Orange
				: (TopPanelBlindState.value ? TOP_PANEL_DATA.ColorCorrection.colorBlind : Color.Green),
		)
	} else {
		RectangleDraw.Image(PathX.Images.buff_outline,
			position,
			(timeToEnd > 480) ? Color.Yellow : Color.Red,
		)
	}

	RectangleDraw.Image(
		PathX.Images.icon_roshan,
		position,
		TOP_PANEL_DATA.Sleeper.Sleeping("TOP_PANEL_ROSHAN_ATTACK") ? Color.Red : Color.White,
	)
}
