import { Input, Item, item_tpscroll, VKeys } from "wrapper/Imports"
import { RectangleX } from "X-Core/Imports"
import { TopPanelItemsAllyState, TopPanelItemsEnemyState, TopPanelItemsState, TopPanelItemsStyle, TopPanelItemsStyleAlpha, TopPanelUltimateStyleImage } from "../menu"
import { TopPanel } from "../Service/TopPanel"
import { TOP_PANEL_UNIT } from "../Service/TopPanelUnit"

export const ModuleItems = (Data: TOP_PANEL_UNIT, items: Item[], index: number) => {
	if ((!TopPanelItemsAllyState.value && !Data.hero.IsEnemy())
		|| (!TopPanelItemsEnemyState.value && Data.hero.IsEnemy()))
		return index

	const Ratio = TopPanel.ScreenRatio
	const playerID = Data.PlayerID
	const position = TopPanel.GPBPosition(playerID, Math.floor(15 * Ratio), index + Math.floor(5 * Ratio))
	const filter = items.filter(item => TopPanelItemsState.IsEnabled(item.Name) && !(item instanceof item_tpscroll))
	if (Data.DrawItems(filter, position, TopPanelItemsStyle, TopPanelItemsStyleAlpha))
		index += (25 + (filter.length > 3 ? (filter.length * 3) : 0)) * Ratio

	return index
}

export const ModuleScroll = (Data: TOP_PANEL_UNIT, index: number) => {

	const Ratio = TopPanel.ScreenRatio
	const playerID = Data.PlayerID
	const altKey = Input.IsKeyDown(VKeys.MENU)
	const isSquare = TopPanelUltimateStyleImage.selected_id !== 0

	const rec3 = TopPanel.GPBUPosition(playerID, Math.floor(46 * Ratio), index + Math.floor(36 * Ratio))

	const HasTPScroll = Data.hero.GetItemByClass(item_tpscroll)

	if (!TopPanelItemsState.IsEnabled("item_tpscroll")) {
		if (!Data.hero.IsEnemy() && altKey && HasTPScroll !== undefined)
			return index += (60 + (isSquare ? 10 : 5)) * Ratio

		return index
	}

	if (!Data.hero.IsEnemy() && altKey && HasTPScroll !== undefined)
		return index += 60 * Ratio

	if (Data.hero.IsEnemy() && Data.DrawScroll(HasTPScroll, altKey ? rec3 : new RectangleX(), isSquare))
		index += (50 + (isSquare ? 10 : 5)) * Ratio

	return index
}
