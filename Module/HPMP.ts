import { Color } from "wrapper/Imports"
import { PathX } from "X-Core/Imports"
import { TopPanelBarAllySize, TopPanelBarEnemySize, TopPanelBlindState, TopPanelHealth, TopPanelMana, TopPanelVisibilityDimHPMPState } from "../menu"
import { TopPanel } from "../Service/TopPanel"
import { TOP_PANEL_UNIT } from "../Service/TopPanelUnit"
import { TOP_PANEL_VALIDATE } from "../Service/Validate"

export const ModuleHPMPAllies = (Data: TOP_PANEL_UNIT, index: number) => {
	if (!Data.hero.IsEnemy() && TOP_PANEL_VALIDATE.IsWaitStartGame)
		return index

	const Ratio = TopPanel.ScreenRatio
	const playerID = Data.PlayerID
	const AllySizeScaleBars = TopPanelBarAllySize.value
	const dimHpMp = TopPanelVisibilityDimHPMPState.value

	/** @Health */
	if (TopPanelHealth.TopPanelBarAllyState.value) {

		Data.DrawHealth(
			(!TopPanelBlindState.value ? PathX.Images.topbar_health : PathX.Images.topbar_health_blind),
			dimHpMp,
			TopPanel.GPBPosition(playerID, Math.floor(AllySizeScaleBars * Ratio)),
			Color.White,
			TopPanelMana.TopPanelBarTextAllyState.value,
		)

		if (TopPanelHealth.TopPanelBarTextAllyState.value)
			Data.DrawTextBars(TopPanel.GPBPosition(playerID, Math.floor(AllySizeScaleBars * Ratio)), Data.hero.HP)

		index += AllySizeScaleBars * Ratio
	}

	/** @Mana */
	if (TopPanelMana.TopPanelBarAllyState.value) {
		Data.DrawMana(
			dimHpMp,
			TopPanel.GPBPosition(playerID, Math.floor(AllySizeScaleBars * Ratio), index),
			Color.White /** TopPanelMana.TopPanelBarAllyColor.Color */,
			TopPanelMana.TopPanelBarTextAllyState.value,
		)

		if (TopPanelMana.TopPanelBarTextAllyState.value)
			Data.DrawTextBars(TopPanel.GPBPosition(playerID, Math.floor(AllySizeScaleBars * Ratio), index), Data.hero.Mana)

		index += AllySizeScaleBars * Ratio
	}

	return index
}

export const ModuleHPMPEnemies = (Data: TOP_PANEL_UNIT, index: number) => {
	if (!Data.hero.IsEnemy() && TOP_PANEL_VALIDATE.IsWaitStartGame)
		return index

	const Ratio = TopPanel.ScreenRatio
	const playerID = Data.PlayerID
	const EnemySizeScaleBars = TopPanelBarEnemySize.value
	const dimHpMp = TopPanelVisibilityDimHPMPState.value

	/** @Health */
	if (TopPanelHealth.TopPanelBarEnemyState.value) {

		Data.DrawHealth(
			PathX.Images.topbar_health_dire,
			dimHpMp,
			TopPanel.GPBPosition(playerID, Math.floor(EnemySizeScaleBars * Ratio)),
			Color.White, /* TopPanelHealth.TopPanelBarEnemyColor.Color */
			TopPanelMana.TopPanelBarTextEnemyState.value,
		)

		if (TopPanelHealth.TopPanelBarTextEnemyState.value)
			Data.DrawTextBars(TopPanel.GPBPosition(playerID, Math.floor(EnemySizeScaleBars * Ratio)), Data.hero.HP)

		index += (EnemySizeScaleBars * Ratio)
	}

	/** @Mana */
	if (TopPanelMana.TopPanelBarEnemyState.value) {

		Data.DrawMana(
			dimHpMp,
			TopPanel.GPBPosition(playerID, Math.floor(EnemySizeScaleBars * Ratio), index),
			Color.White, /* TopPanelMana.TopPanelBarEnemyColor.Color */
			TopPanelMana.TopPanelBarTextEnemyState.value,
		)

		if (TopPanelMana.TopPanelBarTextEnemyState.value)
			Data.DrawTextBars(TopPanel.GPBPosition(playerID, Math.floor(EnemySizeScaleBars * Ratio), index), Data.hero.Mana)

		index += (EnemySizeScaleBars * Ratio)
	}

	return index
}
