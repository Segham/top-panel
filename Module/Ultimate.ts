import { Ability, Input, item_tpscroll, VKeys } from "wrapper/Imports"
import { RectangleX } from "X-Core/Imports"
import { TopPanelItemsState, TopPanelUltimate, TopPanelUltimateAllyState, TopPanelUltimateEnemyState, TopPanelUltimateStyleImage } from "../menu"
import { TopPanel } from "../Service/TopPanel"
import { TOP_PANEL_UNIT } from "../Service/TopPanelUnit"

export const ModuleUltimate = (Data: TOP_PANEL_UNIT, index: number, ability: Ability) => {
	if ((!TopPanelUltimateAllyState.value && !Data.hero.IsEnemy())
		|| (!TopPanelUltimateEnemyState.value && Data.hero.IsEnemy()))
		return index

	const playerID = Data.PlayerID
	const altKey = Input.IsKeyDown(VKeys.MENU)
	const showUltCd = TopPanelUltimate.TopPanelBarCooldown.value

	const Ratio = TopPanel.ScreenRatio
	const rec = TopPanel.GPBUPosition(playerID, Math.floor(18 * Ratio))
	const rec2 = TopPanel.GPBUPosition(playerID, Math.floor(46 * Ratio), index + Math.floor(!Data.hero.IsEnemy() ? 32 : 34 * Ratio))
	const isSquare = TopPanelUltimateStyleImage.selected_id !== 0

	const HasTPScroll = Data.hero.GetItemByClass(item_tpscroll)

	if (Data.DrawUltimate(ability, rec,
		(showUltCd && (!altKey || (!TopPanelItemsState.IsEnabled("item_tpscroll") && HasTPScroll === undefined))) ? rec2 : new RectangleX(), isSquare))
		index += (50 + (isSquare ? 10 : 5)) * Ratio

	return index
}
