import { Color, Menu, RendererSDK, Vector2 } from "wrapper/Imports"

const TopPanelTree = Menu.AddEntryDeep(["Visual", "Top Panel"])
export const TopPanelState = TopPanelTree.AddToggle("State", true)
// export const TopPanelOptimizeState = TopPanelTree.AddToggle("Optimize FPS", false, "Option for weak pc")

/** * @Settings */
const TopPanelSettingsTree = TopPanelTree.AddNode("Settings")
export const TopPanelBlindState = TopPanelSettingsTree.AddToggle("Colorblind Mode")

export const TopPanelGraphicsState = TopPanelSettingsTree
	.AddDropdown("Modes graphics", ["Minimal", "Average", "Maximum"], 2, "Option for weak pc")

let isCreateMenuSettings = false
export let TopPanelDebug: Nullable<Menu.Toggle>
export let TopPanelSide: Nullable<Menu.Slider>
export let TopPanelCenter: Nullable<Menu.Slider>
export let TopPanelBottom: Nullable<Menu.Slider>

export function TopPanelCreateSettings() {
	if (isCreateMenuSettings)
		return

	const ScreenSize = RendererSDK.WindowSize
	const Currection = Math.floor(ScreenSize.x * 0.0535)
	const VectorSize = new Vector2(Math.floor(ScreenSize.x * 0.1605), Math.floor(ScreenSize.y * 0.0375))

	if (TopPanelDebug === undefined)
		TopPanelDebug = TopPanelSettingsTree.AddToggle("Show position", false)

	if (TopPanelCenter === undefined)
		TopPanelCenter = TopPanelSettingsTree.AddSlider("Center position", Currection, 0, Math.floor(ScreenSize.x / 4), 2)

	if (TopPanelSide === undefined)
		TopPanelSide = TopPanelSettingsTree.AddSlider("Side position", Math.floor(VectorSize.x), 100, Math.floor(ScreenSize.x / 4), 2)

	if (TopPanelBottom === undefined)
		TopPanelBottom = TopPanelSettingsTree.AddSlider("Bottom position", Math.floor(VectorSize.y), 25, 100, 2)

	isCreateMenuSettings = true
}

/** * @NetWorth */
export const TopPanelNetWorthTree = TopPanelTree.AddNode("Net Worth")
export const TopPanelNetWorthState = TopPanelNetWorthTree.AddToggle("State", true)
export const TopPanelNetWorthColor = TopPanelNetWorthTree.AddColorPicker("Background color", new Color(0, 0, 0, 220))

/** * @Status */
export const TopPanelStatusTree = TopPanelTree.AddNode("Status")
const TopPanelBars = (name: string) => {
	const TopPanelBarTree = TopPanelStatusTree.AddNode(name)
	const TopPanelBarAllyState = TopPanelBarTree.AddToggle("Allies", true)
	const TopPanelBarEnemyState = TopPanelBarTree.AddToggle("Enemies", true)
	const TopPanelBarTextAllyState = TopPanelBarTree.AddToggle("Text ally", true)
	const TopPanelBarTextEnemyState = TopPanelBarTree.AddToggle("Text enemy", true)
	return {
		TopPanelBarTree,
		TopPanelBarEnemyState,
		TopPanelBarAllyState,
		TopPanelBarTextEnemyState,
		TopPanelBarTextAllyState,
	}
}

export const TopPanelStatusSizeTree = TopPanelStatusTree.AddNode("Size Bars MP/HP")
export const TopPanelBarAllySize = TopPanelStatusSizeTree.AddSlider("Allies", 12, 8, 20)
export const TopPanelBarEnemySize = TopPanelStatusSizeTree.AddSlider("Enemies", 12, 8, 20)
export const TopPanelMana = TopPanelBars("Mana")
export const TopPanelHealth = TopPanelBars("Health")

/*** @Lasthits */
const TopPanelStatusTreeLH = TopPanelStatusTree.AddNode("Last hits/denies")
export const TopPanelStatusEnemyLHState = TopPanelStatusTreeLH.AddToggle("Enemies", true)
export const TopPanelStatusAlliesLHState = TopPanelStatusTreeLH.AddToggle("Allies", true)

/** * @BuyBack */
const TopPanelBarBuyBackTree = TopPanelStatusTree.AddNode("Buy Back")
export const TopPanelBarBuyBackState = TopPanelBarBuyBackTree.AddToggle("State", true, "Show outline buy back")
export const TopPanelBarBuyBackCooldown = TopPanelBarBuyBackTree.AddToggle("Cooldown", true, "Show cooldown time")

/** * @Runes */
const TopPanelBarRunesTree = TopPanelStatusTree.AddNode("Runes")
export const TopPanelBarRunesAllyState = TopPanelBarRunesTree.AddToggle("Allies")
export const TopPanelBarRunesEnemyState = TopPanelBarRunesTree.AddToggle("Enemies", true)

/** * @Ultimate */
const TopPanelUltiPortal = (name: string) => {
	const TopPanelBar = TopPanelStatusTree.AddNode(name)
	const TopPanelBarCooldown = TopPanelBar.AddToggle("Cooldown", true)
	return { TopPanelBar, TopPanelBarCooldown }
}

export const UltimateAllyTree = TopPanelStatusTree.AddNode("Ultimate")
export const TopPanelUltimateAllyState = UltimateAllyTree.AddToggle("Allies")
export const TopPanelUltimateEnemyState = UltimateAllyTree.AddToggle("Enemies", true)
export const TopPanelUltimateStyleImage = UltimateAllyTree.AddDropdown("Style image", ["Round", "Square"])
export const TopPanelUltimate = TopPanelUltiPortal("Ultimate")

/** * @Items */
export const TopPanelItemsTree = TopPanelTree.AddNode("Items")
const drawItems: string[] = [
	"item_gem",
	"item_dust",
	"item_rapier",
	"item_aegis",
	"item_cheese",
	"item_tpscroll",
	"item_aeon_disk",
	"item_refresher_shard",
	"item_smoke_of_deceit",
	"item_ward_sentry",
	"item_ward_observer",
	"item_ward_dispenser",
]
export const TopPanelItemsAllyState = TopPanelItemsTree.AddToggle("Show ally items", false, "Show important ally items")
export const TopPanelItemsEnemyState = TopPanelItemsTree.AddToggle("Show enemy items", true, "Show important enemy items")
export const TopPanelItemsStyle = TopPanelItemsTree.AddDropdown("Style image", ["Round", "Square"])
export const TopPanelItemsStyleAlpha = TopPanelItemsTree.AddSlider("Alpha", 255, 50, 255)
export const TopPanelItemsState = TopPanelItemsTree.AddImageSelector("Items", drawItems, new Map(drawItems.map(name => [name, true])))

/** @Visibility */
export const TopPanelVisibilityTree = TopPanelTree.AddNode("Visibility")
export const TopPanelVisibilityFowTimeState = TopPanelVisibilityTree.AddToggle("Time", true, "Show time in fow")
export const TopPanelVisibilityDimHPMPState = TopPanelVisibilityTree.AddToggle("Dim health and mana", true, "Dim health and mana bars when unit is not visible")

/** @ROSHAN */
const TopPanelRoshanTree = TopPanelTree.AddNode("Roshan")
export const TopPanelRoshanState = TopPanelRoshanTree.AddToggle("State", true)
export const TopPanelRoshanRemaining = TopPanelRoshanTree.AddToggle("Show remaining time", true, "Display remaining time first")
export const TopPanelRoshanUseChat = TopPanelRoshanTree.AddToggle("Use chat", true, "When clicking on the roshan icon")
export const TopPanelRoshanaltKey = TopPanelRoshanTree.AddKeybind("Bind", "Alt", "Switch remaining time to respawn")
export const TopPanelRoshanshowMinTime = TopPanelRoshanTree.AddToggle("Min respawn time", true, "Display min time respawn roshan")
export const TopPanelRoshanshowNextItems = TopPanelRoshanTree.AddToggle("Show items", true, "Show next drop items in roshan")
const TopPanelRoshanIMenuPing = TopPanelRoshanTree.AddNode("Settings ping")
export const TopPanelRoshanUseScan = TopPanelRoshanIMenuPing.AddToggle("Use scan", false, "Used scan in roshan position for notify allies")
export const TopPanelRoshanPingSoundVolume = TopPanelRoshanIMenuPing.AddSlider("Volume %", 1, 0, 100, 0, "Ping volume in percent (if 0 = disable)")
export const TopPanelRoshanPingInteravl = TopPanelRoshanIMenuPing.AddSlider("Ping interval", 4, 4, 15, 0, "Intervals ping in roshan position")
export const TopPanelRoshanPing = TopPanelRoshanIMenuPing.AddToggle("Ping in roshan", true, "Visual only for your")
