import { Vector3 } from "wrapper/Imports"

export class TOP_PANEL_UTIL {
	public static sToMin(time: number, chat: boolean = false) {
		time = Math.floor(time)
		return ~~(time / 60) + (!chat ? ":" : "") + (time % 60 < 10 ? "0" : "") + time % 60
	}
	public static RandomVector3(position: Vector3, min: number, max: number) {
		return position.Add(new Vector3().Random(min, max).SetZ(0))
	}
}
