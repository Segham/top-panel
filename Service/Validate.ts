import { DOTA_GameMode, GameRules, npc_dota_hero_meepo, PlayerResource, Unit } from "wrapper/Imports"
import { GameX } from "X-Core/Imports"
import { TopPanelBottom, TopPanelCenter, TopPanelDebug, TopPanelSide, TopPanelState } from "../menu"

export class TOP_PANEL_VALIDATE {

	public static get IsInGame() {
		return TopPanelState.value
			&& GameX.IsInGame
			&& PlayerResource !== undefined
	}

	public static get IsValidSettings() {
		return TopPanelSide !== undefined
			&& TopPanelBottom !== undefined
			&& TopPanelCenter !== undefined
			&& TopPanelDebug !== undefined
	}

	public static get IsWaitStartGame() {
		return GameRules !== undefined && Math.sign(GameRules.GameTime) === -1
			&& (GameRules.GameMode === DOTA_GameMode.DOTA_GAMEMODE_ALL_DRAFT
				|| GameRules.GameMode === DOTA_GameMode.DOTA_GAMEMODE_RD
				|| GameRules.GameMode === DOTA_GameMode.DOTA_GAMEMODE_CM
			)
	}

	public static IsRealHero = (x: Unit) => !x.IsIllusion
		&& !(x instanceof npc_dota_hero_meepo && x.IsClone)
}
