import { Ability, Color, GameState, Hero, Item, item_travel_boots, item_travel_boots_2, Menu, npc_dota_hero_terrorblade, RendererSDK, Team, terrorblade_metamorphosis, Vector2 } from "wrapper/Imports"
import { PathX, PlayerX, RectangleDraw, RectangleX } from "X-Core/Imports"
import { TOP_PANEL_DATA, TOP_PANEL_GRAPHICS } from "../data"
import { TopPanelBlindState, TopPanelGraphicsState } from "../menu"
import { TOP_PANEL_UTIL } from "./Util"

export class TOP_PANEL_UNIT {

	constructor(public hero: Hero) { }

	public get GraphicsState(): TOP_PANEL_GRAPHICS {
		return TopPanelGraphicsState.selected_id
	}

	public get PlayerID() {
		return this.hero.PlayerID
	}

	public DrawMana(dim: boolean = false, position: RectangleX, color: Color = Color.White, stateText: boolean = false) {
		if (!this.hero.IsAlive)
			return
		RectangleDraw.Image(PathX.Images.topbar_mana, position, TOP_PANEL_DATA.ColorCorrection.topbar)
		position.Width *= Math.max((this.hero.ManaPercent / 100), 0.001)
		RectangleDraw.Image(PathX.Images.topbar_mana, position, ((!dim || (this.hero.IsEnemy()
			? this.hero.IsVisible : this.hero.IsVisibleForEnemies)
			? (!stateText ? color : color.SetA(210)) : color.SetColor(100, 100, 150, 255))))
	}

	public DrawHealth(ImageTeam: string, dim: boolean = false, position: RectangleX, color: Color = Color.White, stateText: boolean = false) {
		if (!this.hero.IsAlive)
			return
		RectangleDraw.Image(ImageTeam, position, TOP_PANEL_DATA.ColorCorrection.topbar)
		position.Width *= Math.max((this.hero.HPPercent / 100), 0.001)
		RectangleDraw.Image(ImageTeam, position, ((!dim || (this.hero.IsEnemy()
			? this.hero.IsVisible : this.hero.IsVisibleForEnemies)
			? (!stateText ? color : color.SetA(210)) : color.SetColor(100, 150, 100, 255))))
	}

	public DrawTextBars(position: RectangleX, num: number) {
		if (position.Height <= 6 || !this.hero.IsAlive)
			return
		this.CenterTextRender(Math.round(num).toString(), position, "Calibri", Color.White, false)
	}

	public DrawUltimate(ability: Ability, position: RectangleX, cdPosition: RectangleX, isSquare: boolean = false) {

		const Cooldown = ability.Cooldown
		const CooldownLength = ability.CooldownLength

		if (Math.round(Cooldown) > 0)
			RectangleDraw.Image(PathX.Images.ult_cooldown, position, Color.White.SetA(170))

		const ColorBlind = !TopPanelBlindState.value
			? Color.Green
			: TOP_PANEL_DATA.ColorCorrection.colorBlind

		if (this.hero instanceof npc_dota_hero_terrorblade) {
			const meta_abil = this.hero.GetAbilityByClass(terrorblade_metamorphosis)
			if (meta_abil !== undefined) {
				const MetaCooldown = meta_abil.Cooldown
				const MetaCooldownLength = meta_abil.CooldownLength
				if (meta_abil.Level !== 0) {
					if (Math.round(MetaCooldown) > 0) {
						if (cdPosition.IsZero())
							return false

						if (this.GraphicsState <= TOP_PANEL_GRAPHICS.AVERAGE) {
							RectangleDraw.Image(PathX.DOTAAbilities(ability.Name), cdPosition, Color.White.SetA(230), isSquare ? -1 : 1)
							RectangleDraw.Image(PathX.Images.softedge_circle_sharp, cdPosition, Color.Black.SetA(160), isSquare ? -1 : 1)
							RectangleDraw.Image(PathX.Images.buff_outline, cdPosition, ColorBlind)
						}

						if (this.GraphicsState > TOP_PANEL_GRAPHICS.AVERAGE) {
							RectangleDraw.Image(PathX.DOTAAbilities(ability.Name), cdPosition, Color.White.SetA(230), isSquare ? -1 : 1)
							!isSquare
								? RectangleDraw.Arc(-90, MetaCooldown, MetaCooldownLength, true, cdPosition, Color.Black, Color.Black)
								: RectangleDraw.Radial(-90, MetaCooldown, MetaCooldownLength, true, cdPosition, Color.Black.SetA(175))
						}

						this.CenterTextRender(Math.round(MetaCooldown).toString(), cdPosition)
						return true
					}
				}
			}
		}

		if (Math.round(Cooldown) > 0) {

			if (cdPosition.IsZero())
				return false

			if (this.GraphicsState <= TOP_PANEL_GRAPHICS.MINIMAL)
				RectangleDraw.Image("panorama/images/textures/miniprofile_rock_psd.vtex_c", cdPosition, Color.Black.SetA(160), isSquare ? -1 : 2)

			if (this.GraphicsState === TOP_PANEL_GRAPHICS.AVERAGE) {
				RectangleDraw.Image(PathX.DOTAAbilities(ability.Name),
					cdPosition.Clone().SubtractSizeVector(2.5),
					Color.White.SetA(230),
					isSquare ? -1 : 0,
				)
				RectangleDraw.Image(PathX.Images.softedge_circle_sharp, cdPosition, Color.Black.SetA(160))
				RectangleDraw.Image(PathX.Images.buff_outline, cdPosition, ColorBlind)
			}

			if (this.GraphicsState > TOP_PANEL_GRAPHICS.AVERAGE) {

				RectangleDraw.Image(
					PathX.DOTAAbilities(ability.Name),
					cdPosition.Clone().SubtractSizeVector(2.5),
					Color.White.SetA(230),
					isSquare ? -1 : 0,
				)

				!isSquare
					? RectangleDraw.Arc(-90, Cooldown, CooldownLength, true, cdPosition, Color.Black, Color.Black, ColorBlind)
					: RectangleDraw.Radial(-90, Cooldown, CooldownLength, true, cdPosition, Color.Black.SetA(175), ColorBlind)
			}

			this.CenterTextRender(Math.round(Cooldown).toString(), cdPosition)
			return true
		}

		if (this.hero.Mana < ability.ManaCost && ability.ManaCost !== 0) {
			if (cdPosition.IsZero() || !this.hero.IsAlive)
				return false

			RectangleDraw.Image(PathX.Images.ult_no_mana, position, Color.White.SetA(170))

			const color = new Color(3, 103, 252)
			if (this.GraphicsState <= TOP_PANEL_GRAPHICS.MINIMAL) {
				RectangleDraw.Image("panorama/images/textures/miniprofile_rock_psd.vtex_c", cdPosition, color.SetA(160))
				this.CenterTextRender(Menu.Localization.Localize("No Mana"), cdPosition.SubtractSizeVector(25))
			}

			if (this.GraphicsState === TOP_PANEL_GRAPHICS.AVERAGE) {
				RectangleDraw.Image(PathX.DOTAAbilities(ability.Name),
					cdPosition.Clone().SubtractSizeVector(2.5),
					color.SetA(230),
					isSquare ? -1 : 0,
				)
				RectangleDraw.Image(PathX.Images.softedge_circle_sharp, cdPosition, color.SetA(160))
				RectangleDraw.Image(PathX.Images.buff_outline, cdPosition, color)
			}

			if (this.GraphicsState > TOP_PANEL_GRAPHICS.AVERAGE) {
				RectangleDraw.Image(PathX.DOTAAbilities(ability.Name),
					cdPosition.Clone().SubtractSizeVector(2.5),
					color,
					isSquare ? -1 : 0,
				)
				!isSquare
					? RectangleDraw.Image(PathX.Images.buff_outline, cdPosition, color)
					: RectangleDraw.OutlinedRect(cdPosition, 2, color)
			}

			return true
		}

		if (ability.Level !== 0)
			RectangleDraw.Image((!TopPanelBlindState.value ? PathX.Images.ult_ready : PathX.Images.ult_ready_blind), position, Color.White.SetA(170))

		return false
	}

	public DrawScroll(scroll: Nullable<Item>, cdPosition: RectangleX, isSquare: boolean = false) {
		const icon_item = this.hero.GetItemByClass(item_travel_boots_2)
			|| this.hero.GetItemByClass(item_travel_boots)
			|| scroll

		if (scroll === undefined || icon_item === undefined || cdPosition.IsZero())
			return false

		const Cooldown = scroll.Cooldown
		const CooldownLength = scroll.CooldownLength
		const ColorBlind = !TopPanelBlindState.value
			? Color.Green
			: TOP_PANEL_DATA.ColorCorrection.colorBlind

		if (Math.round(Cooldown) > 0) {

			if (this.GraphicsState <= TOP_PANEL_GRAPHICS.AVERAGE) {
				RectangleDraw.Image(PathX.DOTAItems(icon_item.Name),
					cdPosition.Clone().SubtractSizeVector(2.5),
					Color.White.SetA(230),
					isSquare ? -1 : 0,
				)
				RectangleDraw.Image(PathX.Images.softedge_circle_sharp, cdPosition, Color.Black.SetA(160), isSquare ? -1 : 1)
				RectangleDraw.Image(PathX.Images.buff_outline, cdPosition, ColorBlind)
			}

			if (this.GraphicsState > TOP_PANEL_GRAPHICS.AVERAGE) {
				RectangleDraw.Image(PathX.DOTAItems(icon_item.Name),
					cdPosition.Clone().SubtractSizeVector(2.5),
					Color.White.SetA(230),
					isSquare ? -1 : 0,
				)
				!isSquare
					? RectangleDraw.Arc(-90, Cooldown, CooldownLength, true, cdPosition, Color.Black, Color.Black, ColorBlind)
					: RectangleDraw.Radial(-90, Cooldown, CooldownLength, true, cdPosition, Color.Black.SetA(175), ColorBlind)
			}

			this.CenterTextRender(Math.round(Cooldown).toString(), cdPosition)
			return true
		}

		if (this.hero.Mana < scroll.ManaCost) {
			if (!this.hero.IsAlive)
				return false

			RectangleDraw.Image(
				PathX.DOTAItems(icon_item.Name),
				cdPosition.Clone().SubtractSizeVector(2.5),
				new Color(3, 103, 252),
				isSquare ? -1 : 0,
			)

			!isSquare
				? RectangleDraw.Image(PathX.Images.buff_outline, cdPosition, new Color(3, 103, 252))
				: RectangleDraw.OutlinedRect(cdPosition, 2, new Color(3, 103, 252))

			return true
		}

		RectangleDraw.Image(
			PathX.DOTAItems(icon_item.Name),
			cdPosition.Clone().SubtractSizeVector(2.5),
			Color.White,
			isSquare ? -1 : 0,
		)

		!isSquare
			? RectangleDraw.Image(PathX.Images.buff_outline, cdPosition, ColorBlind)
			: RectangleDraw.OutlinedRect(cdPosition, 2, ColorBlind.SetA(210))

		return true
	}

	public DrawItems(items: Item[], position: RectangleX, MenuStyle: Menu.Dropdown, Alpha: Menu.Slider) {

		let vector = new Vector2(position.X, position.Y)
		const num = position.Width * 0.3
		const vectorSize = new Vector2(num, num)
		const Colors = Color.White.SetA(Alpha.value)
		const IsItem = items.length !== 0

		items.forEach(item => {
			RendererSDK.Image(
				PathX.DOTAItems(item.Name),
				vector,
				MenuStyle.selected_id === 0 ? 0 : -1,
				vectorSize,
				Colors,
			)

			if (MenuStyle.selected_id === 0)
				RendererSDK.Image(PathX.Images.buff_outline, vector, -1, vectorSize, Colors)

			vector.AddForThis(new Vector2(num + 2, 0))

			if (vector.x + num > position.Right)
				vector = new Vector2(position.X, position.Y + num + 2)
		})
		return IsItem
	}

	public ForceDrawBuyback(position: RectangleX, deadPosition: RectangleX) {
		const playerX = PlayerX.DataX.get(this.hero)
		if (playerX === undefined)
			return

		if ((!this.hero.IsEnemy() && !this.hero.IsAlive) || !playerX.HasBuyback)
			return

		if (!this.hero.IsAlive) {
			RectangleDraw.Image(PathX.Images.buyback_header, deadPosition)
			return
		}

		RectangleDraw.Image(PathX.Images.buyback_header, position.MoveBottomBorder(-2))
	}

	// TODO Improved: code rew
	public ForceDrawBuybackCooldown(position: RectangleX, index: number, CooldownState: Menu.Toggle) {
		const playerX = PlayerX.DataX.get(this.hero)
		if (playerX === undefined)
			return false

		const CooldownSleep = PlayerX.Sleeper.RemainingSleepTime(`PLAYER_X_BUY_BACK_${this.PlayerID}`) / 1000
		if (CooldownSleep <= 0 || playerX.HasBuyback || !CooldownState.value)
			return false

		const HeightPos = position.Height / 2
		const IPosvector = new Vector2(position.X, (position.Y + (index / (this.hero.IsAlive ? 1.2 : .90))))
		RendererSDK.FilledRect(IPosvector, new Vector2(position.Width, HeightPos), Color.Black.SetA(180))
		RendererSDK.Image(PathX.Images.buyback_header, IPosvector, -1, new Vector2(position.Width, HeightPos))

		const CooldownMinStr = TOP_PANEL_UTIL.sToMin(CooldownSleep)
		const TextSizeCooldown = IPosvector.AddScalarX((position.Width -
			RendererSDK.GetTextSize(CooldownMinStr, "Calibri", position.Height / 2).x) / 2)

		RendererSDK.Text(CooldownMinStr, TextSizeCooldown.AddScalarY(2), Color.White, "Calibri", position.Height / 2)

		const Size_icon_gold = new Vector2(position.Width / 3.72, position.Height / 1.74)
		const position_icon_gold = new Vector2(position.X + (position.Width / 2.95), position.Y + (position.Height / 2) + (index / (this.hero.IsAlive ? 1.2 : .90)))
		RendererSDK.Image(PathX.Images.gold_large, position_icon_gold, -1, Size_icon_gold)
		return true
	}

	public LastHitDeny(position: RectangleX, ratio: number) {
		const data = PlayerX.DataX.get(this.hero)
		if (data === undefined)
			return

		const Size = Math.floor(16 * ratio)
		const vector = new Vector2(position.X, position.Y + (position.Height / 5))

		this.hero.Team === Team.Dire
			? vector.AddScalarX(5)
			: vector.SubtractScalarX(3)

		RendererSDK.Text(`${data.LastHit}/${data.Denying}`, vector, Color.White, "Calibri", Size)
	}

	public DrawFowTime(position: RectangleX, ration: number) {

		let strTime: Nullable<string>
		const BecameDormantTime = this.hero.BecameDormantTime
		if (BecameDormantTime <= 0 || this.hero.IsVisible)
			return

		if (this.hero.Team === Team.Radiant)
			position.Width *= 0.88

		const num = Math.round(GameState.RawGameTime - (this.hero.IsAlive ? BecameDormantTime : this.hero.RespawnTime))

		if (num > 60)
			strTime = TOP_PANEL_UTIL.sToMin(num)

		const vector = new Vector2(position.X, position.Y + (position.Height / 5))

		if (this.hero.IsEnemy()) {
			vector.AddScalarX(5)
		} else {
			vector.SubtractScalarX(3)
		}

		RendererSDK.Text(strTime !== undefined ? strTime : num.toString(),
			vector,
			Color.White,
			"Calibri",
			Math.floor(17 * ration),
		)
	}

	public DrawRunes(position: RectangleX) {

		let vector = new Vector2(position.X, position.Y)
		const num = position.Width * 0.4
		const vectorSize = new Vector2(num, num)
		let IsRune = false
		TOP_PANEL_DATA.ModifiersRuneMap.forEach((rawGame, modifire) => {
			if (modifire.Caster !== this.hero) {
				IsRune = false
				return
			}

			const Colors = TopPanelBlindState.value
				? TOP_PANEL_DATA.ColorCorrection.colorBlind : Color.Green
			const NameNormalaze = modifire.Name.replace(/modifier_rune_/gi, "")

			if (modifire.RemainingTime <= 20)
				Colors.SetColor(255, 255, 0)

			if (modifire.RemainingTime <= 3)
				Colors.SetColor(255, 0, 0)

			if (this.hero.IsVisible && modifire.RemainingTime > 0 && this.hero.HasBuffByName(modifire.Name)) {

				const rec = new RectangleX(vector, vectorSize)
				RectangleDraw.Image(PathX.Runes(NameNormalaze, true), rec)

				if (this.GraphicsState <= TOP_PANEL_GRAPHICS.AVERAGE)
					RectangleDraw.Image(PathX.Images.buff_outline, rec, Colors)

				if (this.GraphicsState > TOP_PANEL_GRAPHICS.AVERAGE) {
					RectangleDraw.Arc(-90,
						modifire.RemainingTime,
						modifire.Duration,
						true,
						new RectangleX(vector, vectorSize),
						Color.Black,
						Color.Black,
						Colors,
						false,
					)
				}

				vector.AddForThis(new Vector2(num + 2, 0))
				if (vector.x + num > position.Right)
					vector = new Vector2(position.X, position.Y + num + 2)

				IsRune = true
				return
			}

			if (GameState.RawGameTime > rawGame || modifire.RemainingTime <= 0) {
				TOP_PANEL_DATA.ModifiersRuneMap.delete(modifire)
				IsRune = false
				return
			}

			const rec2 = new RectangleX(vector, vectorSize)
			RectangleDraw.Image(PathX.Runes(NameNormalaze, true), rec2)

			if (this.GraphicsState <= TOP_PANEL_GRAPHICS.AVERAGE)
				RectangleDraw.Image(PathX.Images.buff_outline, rec2, Colors)

			if (this.GraphicsState > TOP_PANEL_GRAPHICS.AVERAGE) {
				RectangleDraw.Arc(-90,
					modifire.RemainingTime,
					modifire.Duration,
					true,
					new RectangleX(vector, vectorSize),
					Color.Black,
					Color.Black,
					Colors,
					false,
				)
			}

			vector.AddForThis(new Vector2(num + 2, 0))
			if (vector.x + num > position.Right)
				vector = new Vector2(position.X, position.Y + num + 2)

			IsRune = true
		})
		return IsRune
	}

	private CenterTextRender(text: string, position: RectangleX, fontName: string = "Calibri", color: Color = Color.White, textDivide: boolean = true) {
		const textSize = textDivide ? position.Height / 2 : position.Height
		const pos = Vector2.FromVector3(RendererSDK.GetTextSize(text, fontName, textSize))
			.MultiplyScalarForThis(-1)
			.AddScalarX(position.Width)
			.AddScalarY(position.Height)
			.DivideScalarForThis(2)
			.AddScalarX(position.X)
			.AddScalarY(position.Y)
			.RoundForThis()
		RendererSDK.Text(text, pos, color, fontName, textSize)
	}
}
