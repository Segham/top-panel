import { RendererSDK, Team, Vector2 } from "wrapper/Imports"
import { RectangleDraw, RectangleX } from "X-Core/Imports"

export class TopPanel {

	public static get ScreenSize() {
		return RendererSDK.WindowSize
	}

	public static get ScreenRatio() {
		return RendererSDK.WindowSize.y / 1080
	}

	public static get GetTimePosition() {
		return this.centerPanel.Clone().MultiplySizeF(new Vector2(0.4, 1))
	}

	public static SideOnValueChange(value: number) {
		this.leftPanel.X = (this.ScreenSize.x / 2) - (this.centerPanel.Width / 2) - value
		this.leftPanel.Width = value
		this.rightPanel.Width = value
		this.widthPerPlayer = this.leftPanel.Width / this.PlayersPerPanel
	}

	public static BotOnValueChange(value: number) {
		this.leftPanel.Height = value
		this.rightPanel.Height = value
		this.centerPanel.Height = value
	}

	public static CenterOnValueChange(value: number) {
		this.centerPanel.X = (this.ScreenSize.x / 2) - value
		this.centerPanel.Width = (value * 2)
		this.rightPanel.X = this.centerPanel.Right
		this.leftPanel.X = (this.ScreenSize.x / 2) - (this.centerPanel.Width / 2) - this.leftPanel.Width
		this.widthPerPlayer = this.leftPanel.Width / this.PlayersPerPanel
	}

	public static DebugDraw() {
		RectangleDraw.OutlinedRect(this.leftPanel)
		RectangleDraw.OutlinedRect(this.rightPanel)
		RectangleDraw.OutlinedRect(this.centerPanel)
	}

	public static GetScorePosition(team: Team): RectangleX {
		return team === Team.Radiant
			? new RectangleX(new Vector2(this.centerPanel.X, this.centerPanel.Y),
				new Vector2(Math.floor(this.centerPanel.Width * 0.32), this.centerPanel.Height))
			: new RectangleX(new Vector2(this.centerPanel.Right - Math.floor(this.centerPanel.Width * 0.31), this.centerPanel.Y),
				new Vector2(Math.floor(this.centerPanel.Width * 0.32), this.centerPanel.Height))
	}

	public static GPPosition(ID: number) {
		const position = new RectangleX()
		const panel = ID < this.PlayersPerPanel ? this.leftPanel : this.rightPanel

		if (ID >= this.PlayersPerPanel) {
			ID -= this.PlayersPerPanel
		}

		position.Size = new Vector2(this.widthPerPlayer, panel.Height)
		position.Position = new Vector2(panel.Left + (position.Width * ID), panel.Y)

		return position
	}

	public static GPBUPosition(ID: number, size: number /** float */, topIndent: number /** float */ = 0) {
		const position = new RectangleX()
		const panel = ID < this.PlayersPerPanel ? this.leftPanel : this.rightPanel

		if (ID >= this.PlayersPerPanel) {
			ID -= this.PlayersPerPanel
		}

		position.Size = new Vector2(size, size)
		position.Position = new Vector2((panel.Left + (this.widthPerPlayer * ID) + (this.widthPerPlayer / 2)) - (size / 2), (panel.Height - (size / 2)) + topIndent)

		return position
	}

	public static GPBPosition(ID: number, height: number /** float */, topIndent: number  /** float */ = 0) {
		const position = new RectangleX()
		const panel = ID < this.PlayersPerPanel ? this.leftPanel : this.rightPanel

		if (ID >= this.PlayersPerPanel) {
			ID -= this.PlayersPerPanel
		}

		position.Size = new Vector2(this.widthPerPlayer - 2, height)
		position.Position = new Vector2(panel.Left + ((position.Width + 2) * ID) + 1, panel.Height + topIndent)

		return position
	}

	public static GSPosition(team: Team) {

		const CenterXY = new Vector2(this.centerPanel.X, this.centerPanel.Height)
		const CenterWH = new Vector2(this.centerPanel.Width * 0.32, this.centerPanel.Height)
		const CenterWR = new Vector2(this.centerPanel.Right - (this.centerPanel.Width * 0.31), this.centerPanel.Y)

		if (team === Team.Radiant)
			return new RectangleX(CenterXY, CenterWH)

		return new RectangleX(CenterWR, CenterWH)
	}

	private static PlayersPerPanel = 5
	private static widthPerPlayer: number = 0
	private static leftPanel = new RectangleX()
	private static rightPanel = new RectangleX()
	private static centerPanel = new RectangleX()
}
