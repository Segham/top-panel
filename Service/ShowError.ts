import { Color, GameSleeper, Menu, RendererSDK, Vector2 } from "wrapper/Imports"

export class TOP_PANEL_ERROR {

	public static ShowError = false
	public static Sleeper = new GameSleeper()

	public static TimeError(message: string, second: number) {
		const time = (second * 1000)
		if (this.Sleeper.RemainingSleepTime("TOP_PANEL_ERROR") <= 0 && this.ShowError) {
			this.Sleeper.FullReset()
			return
		}

		this.DrawTextWithBackground(
			Menu.Localization.Localize(message),
			23,
			RendererSDK.WindowSize.DivideScalar(100).MultiplyScalarX(10),
			Color.White,
			Color.Black,
		)

		if (this.ShowError)
			return

		this.Sleeper.Sleep(time, "TOP_PANEL_ERROR")
		this.ShowError = true
	}

	public static DrawTextWithBackground(
		text: string,
		size: number,
		position: Vector2,
		RendererSDKTextColor?: Color,
		RendererSDKRectColor?: Color,
	) {
		const textSize = Vector2.FromVector3(RendererSDK.GetTextSize(text, "Calibri", size))
		RendererSDK.FilledRect(position, textSize.AddScalarY(size / 1.4), RendererSDKRectColor)
		RendererSDK.Text(text, position, RendererSDKTextColor, "Calibri", size)
	}

}
